#!/usr/bin/env ruby

# dir with things like node_data_10289
def load_domain_data(dirpath)
  theme_domains = {}
  Dir[dirpath + "/node_data_*"].each do |file|
    theme = file.split("_")[-1]
    # domain total_range motif_ranges motif_names
    # e1edgA1 62-126 62-126 m1
    theme_domains[theme] =
      File.readlines(file).map { |l| l.split[0] } - ["domain"]
  end
  theme_domains
end

def load_theme_data(filepath)
  # domain theme color reslist
  #   0      1    2      3
  # e1edgA1 27 red 62-126
  hsh = {}
  File.readlines(filepath)[1..-1].each do |line|
    ar = line.split
    hsh[ar[0]] = [] unless hsh[ar[0]]
    hsh[ar[0]] << ar[1]
  end
  hsh
end

# main
theme_domains = load_domain_data(ARGV[0])
domain_themes = load_theme_data(ARGV[1])

# make sure each domain in a theme has the theme in its theme list:
theme_domains.each do |theme, t_domains|
  t_domains.each do |domain|
    unless domain_themes[domain]
      puts "no themes for #{domain}"
      next
    end
    puts "#{theme} !in #{domain}" unless domain_themes[domain].include?(theme)
  end
end

# make sure each theme in a domain theme list has the domain in its network
domain_themes.each do |domain, d_themes|
  d_themes.each do |theme|
    unless theme_domains[theme]
      puts "no domains for #{theme}"
      next
    end
    puts "#{domain} !in #{theme}" unless theme_domains[theme].include?(domain)
  end
end

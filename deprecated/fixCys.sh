#!/bin/sh

#transform network file to sif file
cat network.txt | grep -v 'edgeid chain1' | awk -F' ' '{print $2,$1,$3}' > network.sif

#tell cytoscape to delete the current session and load a new sif
python /home/wwwdata/restJson.py #will also bring a new session as save.cys, locks!

cp /tmp/save.cys ./
unzip save.cys
rm -f save.cys
#inject cytostruct 
cp -r /home/wwwdata/R4M_DATA_PATH Cyto*/apps/
zip -o session.zip -r Cyto*
mv session.zip $1.cys
rm -rf Cyto*


import os
import fcntl
import requests                                          
import json                                              

PORT_NUMBER = 1234
BASE = 'http://localhost:' + str(PORT_NUMBER) + '/v1/'
HEADERS = {'Content-Type': 'application/json'}  

class Lock:
    
    def __init__(self, filename):
        self.filename = filename
        # This will create it if it does not exist already
        self.handle = open(filename, 'w')
    
    # Bitwise OR fcntl.LOCK_NB if you need a non-blocking lock 
    def acquire(self):
        fcntl.flock(self.handle, fcntl.LOCK_EX)
        
    def release(self):
        fcntl.flock(self.handle, fcntl.LOCK_UN)
        
    def __del__(self):
        self.handle.close()



# Small utility function to create networks from list of URLs
def create_from_list(network_list):
    server_res = requests.post(BASE + 'networks?source=url&collection=ppi', data=json.dumps(network_list), headers=HEADERS)
    return json.loads(server_res.content)

def store_file(file):
    server_res = requests.get(BASE + 'session?file='+file, headers=HEADERS)
    return json.loads(server_res.content)

def get_net_id():
    server_res = requests.get(BASE + 'networks', headers=HEADERS)
    return json.loads(server_res.content)[0]

def apply_layout():
    server_res = requests.get(BASE + 'apply/layouts/force-directed/' + str(get_net_id()) )
    return json.loads(server_res.content)

def del_all():
    server_res = requests.delete(BASE + 'session')
    print(server_res.content)
    #server_res = requests.delete(BASE + 'networks/' + str(get_net_id()) + '/views/')
    #print(server_res.content)
    server_res = requests.delete(BASE + 'networks')
    print(server_res.content)


try:
    lock = Lock("/home/wwwdata/.lock_cyto")
    print("try lock")
    lock.acquire()
    print("acquired")
    filepath = os.path.abspath('network.sif')
    network_files = [ 'file://' + filepath ]
    del_all()
    print(json.dumps(create_from_list(network_files), indent=4))
    print(json.dumps(apply_layout() ))
    print(json.dumps(store_file('/tmp/save.cys')))
finally: 
    lock.release()

#include "rapidxml/rapidxml.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <cstring>

using namespace rapidxml;

typedef xml_document<> Xml;
typedef xml_node<> Node;

//globals
float AREA_THRESHOLD(100.0);

int fatal(const std::string& err) { std::cout << err << std::endl; return -1; }
//======================================================================
std::vector<char>* parseFile(const std::string& fname, Xml& doc) {
	using namespace std;
	ifstream theFile (fname.c_str());
	if(theFile.bad()) {
		fatal("can't load file");
		return nullptr;
	}
	else {	
		vector<char>* persistentBuffer = new vector<char>((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
		persistentBuffer->push_back('\0');
		doc.parse<0>(&persistentBuffer->at(0)); 	// Parse the buffer using the xml file parsing library into doc 
		return persistentBuffer;
	}
}
//======================================================================
inline Node* son(Node* node, const char* id) { return node->first_node(id); }
inline const char* val(Node* node, const char* id) { return node->first_node(id)->value(); }
//======================================================================
/*
#interface
#  id
#  area
#  molecule    //left
#    id        //1
#    chain_id
#    class     //we need it to be 'Protein'
#    residues
#       residue
#         solv_en //we need to be > 0.0
#         name    //tla
#         seq_no  //pdb index
#         bonds   // HS, etc.
#         ser_num
#       residue
#         ...
#  molecule //right
#    id //2
*/
//======================================================================
void handleResidue(Node* node, std::vector<int>& out) {
	double solvEnergy = atof(val(node,"solv_en"));
	if(solvEnergy>0.0)
		out.push_back(atoi(val(node,"seq_num")) );
}
//======================================================================
std::vector<int> handleMolecule(Node* node) {
	std::vector<int> ret;
	Node* inode = son(son(node,"residues"),"residue");
	for(; inode; inode = inode->next_sibling()) 
		handleResidue(inode,ret);
	return ret;
}
//======================================================================
typedef std::tuple<double,std::string,std::string,std::vector<int>,std::vector<int> > interfc;

interfc parseInterface(int id, Node* node) {
	double area = atof(val(node,"int_area"));
	
	if(area<AREA_THRESHOLD)
		return std::make_tuple(area,std::string(),std::string(),std::vector<int>(),std::vector<int>());
	
	//there should be 2 molecules
	Node* m1 = son(node,"molecule");
	Node* m2 = m1->next_sibling();
	
	const std::string c1(val(m1,"chain_id"));
	const std::string c2(val(m2,"chain_id"));
	
	if(strcmp(val(m1,"class"),"Protein") || strcmp(val(m2,"class"),"Protein")) //skip ligands and such
		return std::make_tuple(0,c1,c2,std::vector<int>(),std::vector<int>());

	auto v1 = handleMolecule(m1);	
	auto v2 = handleMolecule(m2);	
	if(v1.empty() || v2.empty())
		return std::make_tuple(0,c1,c2,std::vector<int>(),std::vector<int>());

	return std::make_tuple(area,c1,c2,v1,v2);
}
//======================================================================
const void printResV(std::vector<int> & res, std::ostream& out = std::cout) {
    auto rLast = res.back();
    res.pop_back();
	for(auto r : res) 
		out << r << ",";
	out << rLast;
	res.push_back(rLast);	
}
//======================================================================
int parseXml(const std::string& fname) {
	Xml doc;    
	std::unique_ptr<std::vector<char> > bufferKept(parseFile(fname,doc));
	
	Node* root_node = doc.first_node("pdb_entry");
	if(root_node == 0) 
		return fatal("no pdb_entry root");

	std::cout << "edgeid chain1 chain2 area res1 res2\n";
	
	Node* inode = son(root_node,"interface");
	for(; inode; inode = inode->next_sibling()) {
		int id = atoi(val(inode,"id"));
		auto parsedInterface = parseInterface(id, inode);
		double area = std::get<0>(parsedInterface);
		if(area < AREA_THRESHOLD)
			continue;
		
		std::cout << id << " " 
				  << std::get<1>(parsedInterface) << " "
				  << std::get<2>(parsedInterface) << " "
				  << area << " ";	
		printResV(std::get<3>(parsedInterface));
		std::cout << " ";
		printResV(std::get<4>(parsedInterface));
		std::cout << "\n";
		
	}
	return 0;
}
//======================================================================	
int main(int argc, char *argv[]) {
	if(argc<3) {
		std::cout << "Usage: " << argv[0] << " [file-to-parse] [area-threshold]\n";
		return -1;
	}
	
	const auto INPUT_FILE = std::string(argv[1]);
	AREA_THRESHOLD = atof(argv[2]);
	std::cerr << "running with area threshold set to: " << AREA_THRESHOLD << std::endl;
	return parseXml(INPUT_FILE);
}

#!/usr/bin/env ruby

# edge_data_10075:
# key domain1 domain2 range1 range2
# 2 e2z4uA1 e2g5xA1 158-197 155-193
# 8 e2z4uA1 e3ctkA1 158-197 150-188
# ...

# node_data_10075:
# domain total_range motif_ranges motif_names
# e1rl0A1 162-196 162-196 m1

def e_path(epath, id = "")
  epath + "edge_data_" + id
end

def n_path(epath, id = "")
  epath + "node_data_" + id
end

# directory has edge_data_* and node_data_* files
# prepare theme hash
def init_themes_from_path(epath = ARGV[0] + "/")
  themes = {}
  Dir[epath + "node_data_*"].each do |fname|
    id = fname.split("_")[-1]
    if File.exist?(e_path(epath, id)) && File.exist?(n_path(epath, id))
      themes[id] = []
    else STDERR.puts "Skipping #{id}"
    end
  end
  themes
end

# produce output for embedding in themes
# and the .xy file content
# themes:  { 'name' : '10075', 'hrf' : "loadMolV('10075')",
#            'alias' : 'a+b two layers', 'nodes' : 8,
#            'secret' : 'e1rl0A1,e2g5xA1,...'}
def theme_from_data(id, label, edge_lines, nodes_lines)
  nodes = nodes_lines.map { |l| l.split[0] }.join(",")
  result = ["'name' : '#{id}'", # {}"'hrf' : \"loadMolV('#{id}')\"",
            "'alias' : '#{label}'", "'nodes' : #{nodes_lines.size}",
            "'secret' : '#{nodes}'"]
  ["{#{result.join(', ')}}", xy_from_data(edge_lines, nodes_lines)]
end

CYTOXY = <<-STRING
  {
  "format_version" : "1.0",
  "generated_by" : "cytoscape-3.1.1",
  "target_cytoscapejs_version" : "~2.1",
  "data" : {
    "selected" : true,
    "__Annotations" : [ "" ],
    "shared_name" : "network.sif",
    "SUID" : 52,
    "name" : "network.sif"
  },

  "elements" : {
STRING
         .freeze

def node_from_line(line)
  arr = line.split
  ["{", "\"data\" : {\"id\" : \"#{arr[0]}\", \"totalrange\" : \"#{arr[1]}\"",
   "}, \"position\" : {\"x\" : 0, \"y\" : 0} }"].join(" ")
end

def edge_from_line(line)
  arr = line.split
  "{ \"data\": { " +
    %w[id source target range1 range2].zip(arr)
                                      .map { |k, v| "\"#{k}\" : \"#{v}\"" }
                                      .join(", ") + "} }"
end

# embed data in cytoscape.js template
def xy_from_data(edge_lines, nodes_lines)
  nodes = nodes_lines.map { |nl| node_from_line nl }.join(",\n")
  nodes = "\"nodes\" :  [\n#{nodes}\n]"

  edges = edge_lines.map { |el| edge_from_line el }.join(",\n")
  edges = "\"edges\" : [\n#{edges}\n]"

  [CYTOXY, nodes, ",", edges, "}}"].join("\n")
end

# return all lines of a file, but the first one
def lines_minus_header(filepath)
  File.readlines(filepath)[1..-1]
end

# load mapping of themes to multi-word descriptions
def load_theme_labels(filepath)
  themes = {}
  File.readlines(filepath).each do |l|
    ar = l.split
    themes[ar[0]] = ar[1..-1].join(" ").tr("_", " ")
  end
  themes
end

# write data to file
def write_xy(id, xydata, dest_dir)
  File.open(dest_dir + "/" + id + ".xy", "w") { |f| f.puts(xydata) }
end

USAGE = <<-STRING
  Usage:
  arg0 - path to edge/node data
  arg1 - dest path for .xy files
  arg2 - theme labels
STRING
        .freeze

def main(epath, dest_path, theme_labels_path)
  themes = init_themes_from_path epath
  STDERR.puts themes.size

  labels = load_theme_labels theme_labels_path
  values_arr = []
  themes.keys.each do |id|
    edge_lines = lines_minus_header(e_path(epath, id))
    next if edge_lines.empty?
    theme, xy = theme_from_data(id, labels[id], edge_lines,
                                lines_minus_header(n_path(epath, id)))
    values_arr << theme
    write_xy(id, xy, dest_path)
  end
  puts "values=[", values_arr.join(",\n"), "];"
end

if ARGV.size < 3
  puts USAGE
else
  epath = ARGV[0] || "./"
  dest_path = ARGV[1] || epath
  theme_labels_path = ARGV[2]
  main(epath, dest_path, theme_labels_path)
end

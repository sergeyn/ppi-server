#!/usr/bin/env ruby

# didn't filter in this file the ones that are less than 25 residues

# domain theme color reslist
#   0      1    2      3
# e1edgA1 27 red 62-126
# e1edgA1 545 orange 131-241
# e1edgA1 546 yellow 193-241
# e1edgA1 550 green 54-124

COLOURS = { "hydrogen" => "#8d381c",
            "lightmagenta" => "#ff33cc",
            "lightorange" => "#ffcc7f",
            "lightteal" => "#66b2b2",
            "limon" => "#bfff3f",
            "marine" => "#007fff",
            "nitrogen" => "#3333ff" }.freeze

def prepare_one_theme_of_domain(ar)
  resi = ar[3].gsub(",", " OR ")
  color = COLOURS[ar[2]] || ar[2]
  "{'name':'#{ar[0]}','color': '#{color}','resi': '#{resi}','thms': [#{ar[1]}]}"
end

hsh = {}
File.readlines(ARGV[0])[1..-1].each do |line|
  ar = line.chomp.split
  domain = ar[0]
  # todo: this is the wrong way to calculate length! next unless ar[3].to_i >= 25
  hsh[domain] = [] unless hsh[domain]
  hsh[domain] << prepare_one_theme_of_domain(ar)
end

puts "values = {"
hsh.each { |k, v| puts "'#{k}' : [ #{v.join(',')} ]," }
puts '"dummy" : [] };'
puts "list_vals = ["
sorted_keys = (hsh.keys.sort_by { |k| hsh[k].size }).reverse
puts sorted_keys.map { |k| "{'name' : '#{k}' }" }
                .join(",\n")
puts "];"

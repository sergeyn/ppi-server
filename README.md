# README #

**Backend for the PPI server** 

Currently running on: 
http://trachel-srv.cs.haifa.ac.il/rachel/ppi/

Our server helps you with the exploration of macromolecular interfaces. 
We run the given PDB through PISA (Protein Interfaces, Surfaces, Assemblies) program (a part of CCP4 suite) to obtain the interfaces data in XML format.   
Next, we embed this information into a network compatible with Cytoscape (popular network viewer).  
We provide a fully constructed Cytoscape session with predefined configuration of CyToStruct plug-in that allows the user to view the interfaces in her favorite molecular viewer.

3rd party dependencies:  
1) CCP4 suite (at least PISA)  
2) Javascripted Java for Jsmol  
3) Cytoscape.js    

To actually run a server:  
* Compile pisa-xml-parser and place it in cgi dir  
Edit env.php to set the correct paths (make sure pisa.cfg is correct as well)  
Make sure java and j2s are placed in cgi





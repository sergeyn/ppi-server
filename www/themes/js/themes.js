var stage;
var cytoid;
var resList;
var options = {
    valueNames: [ 'name', { name: 'link', attr: 'href' }, { "name": 'hrf', "attr" : 'onclick'}, 'alias', 'nodes'],
    item: '<li><a class="link name hrf"></a></span><span class="alias"></span><b>(<span class="nodes"></span>)</b></li>'
};
var cy;

function uniq(a) {
    return a.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    })
}

function initAll() {
    //form sortable list
    for(var i in values) { //inject links and onclicks
      values[i]["link"]="themes.html#name="+values[i]["name"];
      values[i]["hrf"]="loadMolV('" + values[i]["name"] + "')";
    }
    var listObj = new List('users', options, values);
    listObj.sort("name", { order: "asc" });

    //init ngl molecular viewer
    stage = new NGL.Stage("viewport",{ backgroundColor: "white" } );
    stage.viewer.setSize(400,400);
    $("#toolbox-tools").css({
        display:        "block",
        top: "-" +$("#cy").height()+"px"
    });

    $("#molvpanel").hide();

    // Add drag and resize option to panel
    $("#toolbox-tools").draggable({
        handle: ".panel-heading",
        stop: function(evt, el) {
        }
    }).resizable({
        handles: "e, w, s, se",
     	//ghost: true,
        stop: function(evt, el) {
            stage.viewer.setSize(el.size.width, el.size.height);
        }
    });

    //Generate a union of terms for lookup
    searchterms = []
    values.forEach(function(val) {
        secrets = val['secret'].split(',')
        secrets.forEach(function(v) {searchterms.push(v);});
        searchterms.push(val['alias'])
    });

    $("#countThemes").text(values.length);

    $( "#searchbox1" ).autocomplete({
        minLength : 2,
        source: uniq(searchterms)
    });

    var vid = location.hash.split('name=')[1];
    if(vid) {
       loadMolV(vid);
    }
}

function locationAfterHash(str) {
    if(history.pushState) {
      history.pushState(null, null, str);
    }
    else {
      location.hash = str;
    }
}

function loadMolV(variationId) {
    var vid = variationId;
    var title = "Theme link: " + vid;
    var hrf = $(location).attr("origin") +
              $(location).attr("pathname") + "#name=" + vid;
    locationAfterHash("#name=" + vid)

    $(document).prop("title", title);
    //$("#vartitle").html($("<a href='"+hrf+"'>"+title+"</a>"));
    $("#users").hide();
    cyInit(variationId);
    stage.removeAllComponents();
    $("#molvpanel").show();
}

function closeMolVPanel() {
    $("#molvpanel").hide();
    $("#users").show();
    $(document).prop('title', "Index");
}

function cyInit(id){
   cytoid = id;
  // get elements json from server side
  //+'&height=400&width='+$("#cy").width()
  //url: 'cgi/getJS_var.php?name='+id,
  var graphP = $.ajax({
    url: 'xy/'+id+'.xy',
    type: 'GET',
    dataType: 'json'
  });

  // when both graph export json and style loaded, init cy
  Promise.all([ graphP ]).then(initCy);
}

function initCy( then ){
  cy = window.cy = cytoscape({
  container: document.getElementById('cy'),
        elements: then[0].elements,

//  textureOnViewport: true,
  hideEdgesOnViewport: true,
  hideLabelsOnViewport: true,
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'content': 'data(id)'
      })
    .selector('edge')
      .css({
        'width': 'mapData(weight, 100, 2000, 1, 8)',
        'line-color': 'mapData(weight, 100, 2000, rgb(200,200,200), rgb(20,20,20))',
		'curve-style': 'haystack'
      })
    .selector('.highlighted')
      .css({
        'background-color': '#61bffc',
        'line-color': '#61bffc',
        'target-arrow-color': '#61bffc',
        'transition-property': 'background-color, line-color, target-arrow-color',
        'transition-duration': '0.5s'
      }),

        layout: {name: 'breadthfirst'//'preset'//,
//		 animate: true,
//		 random: false,
//		 maxSimulationTime: 3000,
 //		 ungrabifyWhileSimulating: true
	},

  ready: function(){}
});

    cy.on('tap', 'node', function(event){
        var node = event.target; //cyTarget;
        var nodeid = node._private.data.id;
        if(event.originalEvent.shiftKey) {
            var ref = "viewer.html#name1="+nodeid+"&res1="+node.data()["totalrange"];
            window.open(ref, '_blank');
        }
        else {
            showNode(cytoid,nodeid,node.data());
        }
    });

    cy.on('cxttap', 'node', function(event) {
      var node = event.target; //cyTarget;
      var nodeid = node._private.data.id;
      var where = "domains.html#name="+nodeid+"&theme="+cytoid;
      if(event.originalEvent.shiftKey) {
          window.open(where, '_blank');
      }
      else {
        location = where;
      }
    });

    cy.on('tap', 'edge', function(event){
        var edge = event.target; //cyTarget;
        var edgeid = edge.id();
        var ed = edge.data();
        if(event.originalEvent.shiftKey) {
            var ref = "viewer.html#name1="+ed["source"]+"&res1="+ed["range1"]+"&name2="+ed["target"]+"&res2="+ed["range2"];
            window.open(ref, '_blank');
        }
        else {
            showEdge(edge.data());
        }
    });
}


function represent(structure, scheme) {
	try{
		structure.addRepresentation("cartoon", { color: scheme } );
 	}
	catch(err) {
		console.log("Can't cartoon");
		try {
			structure.addRepresentation("rope", { color: scheme } );
		}catch(err) {
			console.log("Can't rope");
			structure.addRepresentation("licorice", { color: scheme } );
		}
	}
}

function handleLoad(compList) {
    var scheme1 = NGL.ColormakerRegistry.addSelectionScheme(
                  [["magenta",resList[0]],["grey","*"]],"color1");
    represent(compList[0], scheme1);
    if(compList.length>1) {
        var scheme2 = NGL.ColormakerRegistry.addSelectionScheme(
                   [["cyan",resList[1]],["silver","*"]],"color2");
	represent(compList[1], scheme2);

        compList[0].superpose(compList[1], true, resList[0], resList[1]);
    }
    compList[0].autoView();//centerView(false,resList[0]);
}

function resetStage() {
    stage.removeAllComponents();
}

function gotFromPDB(pdbList) {
    var promiseList = []
    for(var i in pdbList) {
       var pdbData = pdbList[i];
       if(pdbData.length == 0) {
           console.log("Got empty PDB data");
            return;
        }
        promiseList.push(stage.loadFile(
                       new Blob( [ pdbData ], { type: 'text/plain'} ),
                       { ext: "pdb" } ));
    }
    if(pdbList.length == promiseList.length) {
        Promise.all(promiseList).then(handleLoad);
    }
}

function showNode(id,nodeid,nodedata) {
    resetStage();
    resList = [nodedata["totalrange"]];
    Promise.all([ $.get("cgi/getPDB_var.php?name="+nodeid) ])
           .then(gotFromPDB);
};

function showEdge(edgedata) {
    resetStage();
    resList = [edgedata["range1"], edgedata["range2"]];
    Promise.all([ $.get("cgi/getPDB_var.php?name="+edgedata["source"]),
                  $.get("cgi/getPDB_var.php?name="+edgedata["target"])])
           .then(gotFromPDB);
};

function formList(values) {
    $("#countDomains").text("Search and filter among " + values.length + " domains:");
    //form sortable list
    for(var i in values) { //inject links and onclicks
      values[i]["link"]="domains.html#name="+values[i]["name"];
      values[i]["hrf"]="showDNode('" + values[i]["name"] + "')";
    }

    var listObj = new List('users',
    {
    valueNames: [ 'name', { "name": 'link', "attr" : 'href'}, { "name": 'hrf', "attr" : 'onclick'}],
    item: '<li><a class="link name hrf"></a></span></li>'
    }
    , values);
    listObj.sort("name", { order: "asc" });
}
    //init ngl molecular viewer
    stage = new NGL.Stage("viewport",{ backgroundColor: "white" } );
    try {
        stage.viewer.setSize(400,400);
        $("#molvpanel").hide();
    }
    catch(err) {
        console.log(err);
    }


   $("#toolbox-tools").css({
        display:        "block",
        top: "-" +$("#cy").height()+"px"
    });


    // Add drag OR resize option to panel
    $("#toolbox-tools").draggable({
        handle: ".panel-heading",
        stop: function(evt, el) {
        }
    }).resizable({
        handles: "e, w, s, se",
     	//ghost: true,
        stop: function(evt, el) {
            stage.viewer.setSize(el.size.width, el.size.height);
        }
    });

function colorThemes(nodeid, compList) {
    var cboxes = $(':checkbox');
    if(compList[0].reprList[0].length) {
        compList[0].removeRepresentation(compList[0].reprList[0]);
        console.log(compList[0].reprList);
    }
    var hsh = {};
    for(var i in cboxes) {
        if(cboxes[i].name == 'theme' && $(cboxes[i]).prop("checked")) {
            hsh[cboxes[i].value] = true;
        }
    }
    var data = values[nodeid];
    var colorsRes = [];
    for(var i in data) {
        if(data[i].resi in hsh)
            colorsRes.push([data[i]['color'], data[i]['resi']]);
    }

    colorsRes.push(["silver","*"]);
    represent(compList[0],
              NGL.ColormakerRegistry.addSelectionScheme(colorsRes, 'scheme1'));
    compList[0].autoView(); //centerView(false);
};

function themeLinks(themes) {
    var str = "";
    for(var i in themes)
      str += "<a href='themes.html#name="+themes[i]+"'>"+themes[i]+"</a>&nbsp;"
    return str;
}

function getCheckBox(data) {
  return "<input type='checkbox' name='theme' value='"
          + data['resi'] + "'/>" + data['name']
          + "&nbsp;&nbsp;(" + data['resi'] + ") Themes: "
          + themeLinks(data["thms"]) +  "<br />";
}

function populateControls(nodeid, themeid, compList) {
    var el = $("#controls");
    var data = values[nodeid];
    el.empty();
    for(var i in data) {
        el.append($(getCheckBox(data[i])));
    }

    // if themeid given, highlight the first theme_ with it
    if(themeid) {
      var flag = false;
      for(var i in data) {
        if(-1 != $.inArray(themeid, data[i]["thms"])) {
            $("#controls :checkbox:nth-of-type(" + (parseInt(i)+1) +")").prop("checked", true);
            flag = true;
            break;
        }
      }

      if(!flag) {
        $("#controls :first-child").prop("checked", true);
      }

    }
    else {
      $("#controls :first-child").prop("checked", true);
    }

    // setup on-click
    $(':checkbox').click(function(e) {
        if(! e.shiftKey) {
            $(':checkbox').prop("checked", false);
        }
        $(this).prop("checked", true);
        colorThemes(nodeid, compList);
    });
}

function locationAfterHash(str) {
    if(history.pushState) {
      history.pushState(null, null, str);
    }
    else {
      location.hash = str;
    }
}

function handleLoad(nodeid, themeid, compList) {
    $("#molvpanel").show();
    populateControls(nodeid, themeid, compList);

    colors_res = [["silver","*"]];
    represent(compList[0],
              NGL.ColormakerRegistry.addSelectionScheme(colors_res, 'scheme1'));
    colorThemes(nodeid, compList);
}

function gotFromPDB(nodeid, themeid, pdbList) {
    var promiseList = []
    for(var i in pdbList) {
       var pdbData = pdbList[i];
       if(pdbData.length == 0) {
           console.log("Got empty PDB data");
            return;
        }
        promiseList.push(stage.loadFile(
                       new Blob( [ pdbData ], { type: 'text/plain'} ),
                       { ext: "pdb" } ));
    }
    var boundHandleLoad = handleLoad.bind(null, nodeid, themeid);
    Promise.all(promiseList).then(boundHandleLoad);
}

function showDNode(nodeid, themeid) {
    var locationStr = "#name="+nodeid;
    if(themeid) {
      locationStr += "&theme=" + themeid;
    }
    locationAfterHash(locationStr);
    stage.removeAllComponents();
    var boundGotFromPDB = gotFromPDB.bind(null, nodeid, parseInt(themeid));
    Promise.all([ $.get("cgi/getPDB_var.php?name="+nodeid) ])
           .then(boundGotFromPDB);
};

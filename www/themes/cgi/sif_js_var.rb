kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'

def sif2js(path, name)
	sifArr = %x[grep #{name}_ #{path}/variations/themes_by_length_edge_data].split("\n")
	#require 'set'
	#nodes = Set.new()
	edges = []
	#0     1       2    3     4       5       6    
    #key node1 node2 domain1 domain2 range1 range2
    #10075_2 10075_e2z4uA1 10075_e2g5xA1 e2z4uA1 e2g5xA1 158-197 155-193
	sifArr.each do |t|
		edge = t.split
		#nodes.add(edge[3])
		#nodes.add(edge[4])
        id = edge[0].split('_')[-1]
		edges << "{ \"data\": { \"id\": \"#{id}\", \"source\": \"#{edge[3]}\", \"target\": \"#{edge[4]}\", \"range1\" : \"#{edge[5]}\", \"range2\" : \"#{edge[6]}\"} }"
	end
    nodesFile = %x[grep #{name}_ #{path}/variations/themes_by_length_node_data].split("\n")
    nodesS = nodesFile.map {|line|
        #node domain total_range motif_ranges motif_names
        #10075_e1rl0A1 e1rl0A1 162-196 162-196 m1
        arr = line.split
        n,tr = arr[1],arr[2]
        "{ \"data\": { \"id\": \"#{n}\", \"totalrange\" : \"#{tr}\" }, \"position\": {\"x\": 0, \"y\": 0} }" 
    }
	return [ "{ \"nodes\": [\n", nodesS.join(",\n"), "],\n\"edges\": [\n", edges.join(",\n"), "]\n}" ].join
end	

#=======================================================================
 
if ARGV.size == 2
puts <<STRING
{
  "format_version" : "1.0",
  "generated_by" : "cytoscape-3.1.1",
  "target_cytoscapejs_version" : "~2.1",
  "data" : {
    "selected" : true,
    "__Annotations" : [ "" ],
    "shared_name" : "network.sif",
    "SUID" : 52,
    "name" : "network.sif"
  },
  
  "elements" :
STRING
   
	puts sif2js(ARGV[0],ARGV[1])
puts "}"

else
	puts "Usage:\n1. sif2js [path to themes-data] [theme-id]"
end

This folder contains a preconfigured Cytoscape session (it is named: your-protein.cys).
Make sure that Cytostruct plug-in is installed in your Cytoscape.

Troubleshooting:
1. If a network is missing from the session file
In the menu select: File->Import->Network->File 
Choose 'network.sif'

2. If no 'CyToStruct' menu is available when right-clicking a node or an edge
Use Apps -> Confiugre CyToStruct wizard to load the confiuration from the file 'pisa.yaml'

For any other issues contact nsergey82@gmail.com

function getChainCenters(jsml) {
	var atOfD = Jmol.getPropertyAsArray(jsml,'atomInfo', 'alpha'); 
	var chainHsh = {}

	var lenA = atOfD.length;
	if(lenA<1) {
		//retry
		console.log('retrying...');
		Jmol.script(jsml,'echo 1');
		Jmol.getPropertyAsArray(jsml,'appletInfo');
		atOfD = Jmol.getPropertyAsArray(jsml,'atomInfo', 'alpha');
		lenA = atOfD.length;
		if(lenA<1) {
			return [0,0,0];
		}
	}
	for(var i = 0; i<lenA; ++i) {
		var atom = atOfD[i];
		var chain = atom.chain;
		if(!(chain in chainHsh))
			chainHsh[chain] = []
		chainHsh[chain].push( [ atom.x, atom.y, atom.z ] );
	}
	res = {}
	for(chain in chainHsh) {
		var sums = [0.0,0.0,0.0];
		var sz = chainHsh[chain].length;
		for(var i=0; i<sz; ++i) {
			sums[0]+=chainHsh[chain][i][0];
			sums[1]+=chainHsh[chain][i][1];
			sums[2]+=chainHsh[chain][i][2];
		}
		res[chain] = [sums[0]/sz, sums[1]/sz, sums[2]/sz];
	}
	return res;
}

function setLayoutAsProjection(cy,centers,scale) {
	if(centers.length == 1 && centers[0] == 0.0)
		return;
	for(chain in centers) { 
		cy.$('#'+chain).position({x: centers[chain][0]*scale, y: centers[chain][1]*scale}) 
	}
	cy.center();
}


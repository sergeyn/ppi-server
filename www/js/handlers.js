function showAndRun(pdbid,scriptText) {
  if(typeof jmolApplet0 === "undefined") {
    initInfo(pdbid);  
  }
  else {
    $("#appdiv").show();
    Jmol.script(jmolApplet0,scriptText);
  }
}

function showNode(pdbid,nodeid) { 
     showAndRun(pdbid,"display *.CA; color gray; select schain1; color gray; select schain2; color gray; define schain *:"+nodeid+"; select schain; color red");	
}

function showEdge(pdbid,srcid,trgtid) {
    if(srcid == trgtid) {
	alert('self loop');
    }
    else {
       showAndRun(pdbid,"display *.CA; color gray; select schain1; color gray; select schain2; color gray;  define schain1 *:"+srcid+"; select schain1; color cyan; define schain2 *:"+trgtid+"; select schain2; color orange;");
    }
}

$(function(){
  // get elements json from server side libcola
  var pdbid = location.search.split('name=')[1];
  var graphP = $.ajax({
	  //http://trachel-srv.cs.haifa.ac.il/rachel/ppi/
    url: 'cgi/getJS_var.php?name='+pdbid+'&height='+$("#cy").height()+'&width='+$("#cy").width(), //
    type: 'GET',
    dataType: 'json'
  });

  // when both graph export json and style loaded, init cy
  Promise.all([ graphP ]).then(initCy);

  function initCy( then ){
    var cy = window.cy = cytoscape({
  container: document.getElementById('cy'),
        elements: then[0].elements,

//  textureOnViewport: true,
  hideEdgesOnViewport: true,
  hideLabelsOnViewport: true, 
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'content': 'data(id)'
      })
    .selector('edge')
      .css({
        'width': 'mapData(weight, 100, 2000, 1, 8)',
        'line-color': 'mapData(weight, 100, 2000, rgb(200,200,200), rgb(20,20,20))',
		'curve-style': 'haystack'
      })
    .selector('.highlighted')
      .css({
        'background-color': '#61bffc',
        'line-color': '#61bffc',
        'target-arrow-color': '#61bffc',
        'transition-property': 'background-color, line-color, target-arrow-color',
        'transition-duration': '0.5s'
      }),

        layout: {name: 'breadthfirst'//'preset'//,
//		 animate: true,
//		 random: false,
//		 maxSimulationTime: 3000, 
 //		 ungrabifyWhileSimulating: true
	},

  ready: function(){}
});

cy.on('tap', 'node', function(event){
    var node = event.cyTarget;
    var pdbid = location.search.split('name=')[1];
    var nodeid = node.id();
    showNode(pdbid,nodeid,node.data());
});

cy.on('tap', 'edge', function(event){
    var edge = event.cyTarget; 
    var pdbid = location.search.split('name=')[1];
    var edgeid = edge.id();
    showEdge(edge.data());
});
  }
});

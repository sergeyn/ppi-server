function initInfo(pdbid) {

  var Info = {
        width: 400,
        height: 400,
        debug: false,
        color: "0xFFFFFF",
        //addSelectionOptions: true,
        use: "HTML5",   // JAVA HTML5 WEBGL are all options
        j2sPath: "./j2s", // this needs to point to where the j2s directory is.
        jarPath: "./java",// this needs to point to where the java directory is.
        jarFile: "JmolAppletSigned.jar",
        isSigned: true,
        script: "load getPDB.php?name="+pdbid+"; set chainCaseSensitive ON; set antialiasDisplay; cartoon only;  hide all;",	
        serverURL: "http://chemapps.stolaf.edu/jmol/jsmol/php/jsmol.php",
        readyFunction: jmol_isReady,
        disableJ2SLoadMonitor: true,
        disableInitialConsole: true,
	coverImage: "./ala.gif",
	deferUncover: true,
	//scriptCallback: "afterscript_callback",
        allowJavaScript: true
        //console: "debugc" // default will be jmolApplet0_infodiv, but you can designate another div here or "none"
  };
  //first call 
  $("#appdiv").html(Jmol.getAppletHtml("jmolApplet0", Info));
  $("#appdiv").hide();

}

Jmol._isAsync = true;

//var s = document.location.search;
//Jmol._debugCode = (s.indexOf("debugcode") >= 0);
jmol_isReady = function(applet) {
	//document.title = (applet._id + " - Jmol " + Jmol.___JmolVersion)
	Jmol._getElement(applet, "appletdiv").style.border="1px solid gray"
}		
//var lastPrompt=0;
$(document).ready(function() { 
	var pdbid = location.search.split('name=')[1];
	initInfo(pdbid); 
} );

function afterscript_callback() {  }

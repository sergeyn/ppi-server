function initInfo(scriptT) {
  var Info = {
        width: $("#toolbox-tools").width()-20,
        height: $("#toolbox-tools").height()-30,
        debug: false,
        color: "0xFFFFFF",
        //addSelectionOptions: true,
        use: "HTML5",   // JAVA HTML5 WEBGL are all options
        j2sPath: "js/j2s", // this needs to point to where the j2s directory is.
        jarPath: "js/java",// this needs to point to where the java directory is.
        //jarFile: "JmolAppletSigned.jar",
        isSigned: true,
        script: scriptT, //"load getPDB_var.php?name="+pdbid+"; set chainCaseSensitive ON; set antialiasDisplay; cartoon only; ",	
        //serverURL: "http://chemapps.stolaf.edu/jmol/jsmol/php/jsmol.php",
        readyFunction: jmol_isReady,
        disableJ2SLoadMonitor: true,
        disableInitialConsole: true,
	//coverImage: "./ala.gif",
	//deferUncover: true,
	scriptCallback: "afterscript_callback",
        allowJavaScript: true
        //console: "debugc" // default will be jmolApplet0_infodiv, but you can designate another div here or "none"
  };
  return Info;
}

Jmol._isAsync = true;

//var s = document.location.search;
//Jmol._debugCode = (s.indexOf("debugcode") >= 0);
jmol_isReady = function(applet) {
	//document.title = (applet._id + " - Jmol " + Jmol.___JmolVersion)
    Jmol.script(jmolApplet0,"compare {1.1} {2.1} SUBSET {*.CA}");
     $("#appdiv").hide();
	Jmol._getElement(applet, "appletdiv").style.border="1px solid gray"
}		
//var lastPrompt=0;

function afterscript_callback() {
    //console.log("afterscript_callback");
}

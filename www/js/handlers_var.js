function showNode(pdbid,nodeid,nodedata) { 
    var script="load cgi/getPDB_var.php?name="+nodeid+"; set chainCaseSensitive ON; set antialiasDisplay; cartoon only; display *.CA; select "+nodedata["totalrange"]+"; color red;";
    $("#appdiv").show();
    Jmol.script(jmolApplet0, script);
}

function showEdge(edgedata) {
    srcid = edgedata["source"];
    trgtid = edgedata["target"];
    if(srcid == trgtid) {
		alert('self loop');
    }
    else {
        var script="load FILES cgi/getPDB_var.php?name="+srcid+" \"cgi/getPDB_var.php?name="+trgtid+"\"; model 0; set chainCaseSensitive ON; set antialiasDisplay; cartoon only; display *.CA; select ("+edgedata["range1"]+" and model=1.1); color cyan; select ("+edgedata["range2"]+" and model=2.1); color magenta; compare {1.1} {2.1} SUBSET {*.CA} rotate translate 0";
        $("#appdiv").show();
        Jmol.script(jmolApplet0, script);		
    }
}

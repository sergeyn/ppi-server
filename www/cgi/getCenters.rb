#1 -  6         Record name   "ATOM  "
#7 - 11         Integer       serial       Atom  serial number.
#13 - 16        Atom          name         Atom name.
#17             Character     altLoc       Alternate location indicator.
#18 - 20        Residue name  resName      Residue name.
#22             Character     chainID      Chain identifier.
#23 - 26        Integer       resSeq       Residue sequence number.
#27             AChar         iCode        Code for insertion of residues.
#31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
#39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
#47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
#55 - 60        Real(6.2)     occupancy    Occupancy.
#61 - 66        Real(6.2)     tempFactor   Temperature  factor.
#77 - 78        LString(2)    element      Element symbol, right-justified.
#79 - 80        LString(2)    charge       Charge  on the atom.

def parseAtomsFromLines lines
	chains = {}
	lines.each {|a|
		next unless a.match(/^ATOM/) 
		next unless a[12..14].include? 'CA'     
		x = a[30..37].strip.to_f
		y = a[38..45].strip.to_f
		z = a[46..53].strip.to_f
		chain = a[21]
		chains[chain] = [] unless chains[chain]
		chains[chain] << [x,y,z]
	}
	chains.keys.each { |chain|
		x = 0
		y = 0
		z = 0
		chains[chain].each { |a| 
			x+=a[0]
			y+=a[1]
			z+=a[2]
		}
		sz = chains[chain].size
		puts "#{chain} #{x/sz} #{-1*y/sz} #{z/sz}"
	}
end

#parseAtomsFromLines File.new(ARGV[0])

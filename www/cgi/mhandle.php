<?php
	include 'env.php';
	include 'utils.php';

	$name = sanitizePDBName($_GET["name"]);
	//------------------------------------------------------
	// 1. see if we have the entire suite ready:
	if(file_exists("$tmpd/$name.zip")) {
		sleep(1);
		echo "cgi/result.php?name=$name";
		exit;
	}
	//------------------------------------------------------
	// 2. see if we have the PDB:
	$file = "$tmpd/$name.pdb";
	if(!file_exists($file))	{  //if no local pdb, try to get it from rcsb
		downloadFile("http://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=pdb&compression=NO&structureId=$name",$file);
	}
	if(!file_exists($file)) {  //DL attempt failed!
		echo "nopdb.html";
		exit;
    }
	//-------------------------------------------------------	
	// 3. We have the PDB, let's see if PISA ran on it
	if (!file_exists("$file.xml")) {
		if(false == runPisaAsync($name,$file)) { //run async
			echo "timeout.html"; //will take some time to finish
			exit;		
		}
	}

	//-------------------------------------------------------	
	// 4. Check if a network file is present (we managed to parse pisa xml):
	if (!file_exists("$tmpd/$name/network.txt")) {
		echo "noparse.html";
		exit;
	}
	
	//-----------------------------------------------------------
	// All in place! Let's make a zip file from this one! 
	makeZip($name,$file);
	if(isset($_GET["up"])) {
		header("Location: result.php?name=$name");
		die();
	}
	echo "cgi/result.php?name=$name";
?>

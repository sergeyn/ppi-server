<?php
function sanitizePDBName($pdbname) {
	return strtoupper(preg_replace('/[^a-zA-Z0-9-_\.]/','',$pdbname)); 
}

function downloadFile($url, $path)
{
    $newfname = $path;
    $file = fopen ($url, 'rb');
    if ($file) {
        $newf = fopen ($newfname, 'wb');
        if ($newf) {
            while(!feof($file)) {
                fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
            }
        }
    }
    if ($file) {
        fclose($file);
    }
    if ($newf) {
        fclose($newf);
    }
}

include 'env.php';

function runPisaAsync($name,$file) {
	system("php runPisa.php $name $file &");// >& /dev/null &");
	$time_pre = microtime(true);
	while(microtime(true)-$time_pre < 20) {
		sleep(1);
		if(file_exists("$file.xml")) {
			return true;
		}
	}
	return file_exists("$file.xml");
}

function makeZip($name,$file) {
	$tmpd = $GLOBALS['tmpd'];
	$cgidir = getcwd();

	system("mv $file $tmpd/$name/protein.pdb; ln -s $tmpd/$name/protein.pdb $file;");
	
	//#transform network file to sif file
	system("cd $tmpd/$name/; cat network.txt | grep -v 'edgeid chain1' | awk -F' ' '{print $2,$1,$3,$4}' > network.sif");
	//system("cd $tmpd/$name/; cat network.txt | grep -v 'edgeid chain1' | awk -F' ' '{print $2,$1,$3,100.0/sqrt($4)}' > netWithW.sif");
	//#use ruby script to build the session from template session
	system("cd $tmpd/$name/; ruby $cgidir/sif_js_xml.rb network.sif $name");
	system("cd $tmpd; zip -r $name.zip $name > /dev/null"); 
}

function send_zip_file( $file )
{
    if ( !is_file( $file ) )
    {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        //require_once("404.php");
        exit;
    }
    elseif ( !is_readable( $file ) )
    {
        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
        //require_once("403.php");
        exit;
    }
    else
    {
	header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
        header("Pragma: public");
        header("Expires: 0");
        header("Accept-Ranges: bytes");
        header("Connection: keep-alive");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-type: application/zip");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
	header('Content-Length: '.filesize($file));
        header("Content-Transfer-Encoding: binary");
	ob_clean(); //Fix to solve "corrupted compressed file" error!
        readfile($file);
    }
}
?>

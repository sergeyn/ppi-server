<?php
	include 'env.php';
	
	$name = $argv[1];
	$file = $argv[2];
	
	//run pisa + xml exporter
	system("$pisa/bin/pisa $name -analyse $file --lig-exclude=all $pisa/pisa.cfg > /dev/null && $pisa/bin/pisa $name -xml interfaces $pisa/pisa.cfg > $file.xml");
	//prepare dir
	system("mkdir $tmpd/$name; cp $zipContent/README.txt $tmpd/$name; cp $zipContent/R4M_DATA_PATH/R4M_DATA_PATH $tmpd/$name/pisa.yaml");
	system("$parsePisaXML $file.xml 100 > $tmpd/$name/network.txt"); //parse the xml
?>

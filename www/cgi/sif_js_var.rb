kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'

def sif2js(path='')
	sifArr = IO.readlines(path)
	require 'set'
	nodes = Set.new()
	edges = []
	#0     1       2        3     4
	#key domain1 domain2 range1 range2
	sifArr[1..-1].each do |t|
		edge = t.split
		nodes.add(edge[1])
		nodes.add(edge[2])
		edges << "{ \"data\": { \"id\": \"#{edge[0]}\", \"source\": \"#{edge[1]}\", \"target\": \"#{edge[2]}\", \"range1\" : \"#{edge[3]}\", \"range2\" : \"#{edge[4]}\"} }"
	end
    nodesFile = IO.readlines(path.sub("edge","node"))
    nodesS = nodesFile[1..-1].map {|line|
        #domain total_range motif_ranges motif_names
        arr = line.split
        n,tr = arr[0],arr[1]
        "{ \"data\": { \"id\": \"#{n}\", \"totalrange\" : \"#{tr}\" }, \"position\": {\"x\": 0, \"y\": 0} }" 
    }
	return [ "{ \"nodes\": [\n", nodesS.join(",\n"), "],\n\"edges\": [\n", edges.join(",\n"), "]\n}" ].join
end	

#=======================================================================
 
if ARGV.size == 1
puts <<STRING
{
  "format_version" : "1.0",
  "generated_by" : "cytoscape-3.1.1",
  "target_cytoscapejs_version" : "~2.1",
  "data" : {
    "selected" : true,
    "__Annotations" : [ "" ],
    "shared_name" : "network.sif",
    "SUID" : 52,
    "name" : "network.sif"
  },
  
  "elements" :
STRING
   
	puts sif2js(ARGV[0])
puts "}"

#elsif ARGV.size == 2	
#	sif2session ARGV[0], TEMPLATE_SESSION, ARGV[1]
else
	puts "Usage:\n1. sif2js -- path to network.sif file\n2. sif2cys -- path to network.sif and pdbID"
end

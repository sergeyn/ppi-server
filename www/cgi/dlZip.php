<?php

	include 'env.php';
	include 'utils.php';

	$name = sanitizePDBName($_GET["name"]);
	$attachment_location = "$tmpd/$name.zip";
	if(file_exists($attachment_location)) {
		send_zip_file($attachment_location);
	}
	else {
		echo ("no zip file :(");
	}
	exit;
?>

<?php

include 'env.php';
include 'utils.php';
	
$target_dir = $tmpd;
$base = basename($_FILES["fileToUpload"]["name"]);
$target_file = $target_dir . $base;
$pdbF = explode('.',$base);
$pdb = strtoupper($pdbF[0]);
$header = "Location: mhandle.php?name=$pdb&up=1";
if (file_exists($target_file)) {
	echo "exists";
	header($header);
	die();
}
if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	if(sizeof($pdbF)==3 && 0 == strcmp($pdbF[2],"gz")) { //handle gzip
		system("gunzip  $target_file");
	}
	if(0!=strcmp($pdb,$pdbF[0])) { //lower-case file
		system("mv  $target_dir/$pdbF[0].$pdbF[1] $target_dir/$pdb.pdb");
	}
	header($header);
	die();
} else {
   die("Sorry, there was an error uploading your file.");
}


?>

<?php
	include 'env.php';
	include 'utils.php';

	$name = sanitizePDBName($_GET["name"]);
    $template = "../js_html.template";
	$homepage = file_get_contents($template);
	if(! file_exists("$tmpd/$name/network.sif")) {
		system("cd $tmpd/$name/; cat network.txt | grep -v 'edgeid chain1' | awk -F' ' '{print $2,$1,$3,$4}' > network.sif");
		echo "Please, process first";
		exit;
	}
	print str_replace("#PDBNAME#", $name, $homepage);
	exit;
?>

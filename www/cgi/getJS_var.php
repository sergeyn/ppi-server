<?php
	include 'env.php';
	include 'utils.php';

	$name = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','',$_GET["name"]));
	$width = intval($_GET["width"]);
	$height = intval($_GET["height"]);

	$fileN = "$tmpd/$name.xy";
	if(!file_exists($fileN) && file_exists("$tmpd/variations/$name")) {
		system("ruby sif_js_var.rb '$tmpd/variations/$name' > $fileN");
	}
	system("cat $fileN"); //or just redirect?
	exit;
?>

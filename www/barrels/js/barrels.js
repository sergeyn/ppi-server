var stage;
var cytoid;
var resList;
var cy;
var removed;
var slider_start = 15;
var g_activeElement = {};

function initAll(nodesjson, stylejson) {
  Promise.all([
    $.getScript("js/libs/list.min.js"),
    $.getScript("js/libs/jquery-ui.min.js"),
    $.getScript("js/libs/ngl.js"),
    $.getScript("js/libs/cytoscape.min.js"),
    $.getScript("js/libs/pako_inflate.min.js"),
    $.getScript("js/libs/nouislider.min.js"),
    $.getScript( "js/compressed.js" )]
  ).then(function() {
    initTransport(); // initialize ajaxTransport to support zipped things

    //init ngl molecular viewer
    stage = new NGL.Stage("viewport",{ backgroundColor: "white" } );
    stage.viewer.setSize(400,400);
    $("#molvpanel").hide();
    // Add drag and resize option to panel
    $("#toolbox-tools").css({display: "block", top: 140, "z-index": 100})
    makeWindow("#toolbox-tools",
               function(evt, el) {
                        stage.viewer.setSize(el.size.width, el.size.height);
               });

    $("#lenswindow").css({display: "block", top: 140, left: $(window).width()-600, "z-index": 100})
    makeWindow("#lenswindow",function(evt, el) {});

    cyInit(nodesjson, stylejson);
    stage.removeAllComponents();
    $("#toolbox-tools").hide();
    $("#molvpanel").show();


      let slider = document.getElementById('slider');
      noUiSlider.create(slider, {
      start: [slider_start],
      range: {
        'min': [   0 ],
        '30%': [  20 ],
        '70%': [ 100 ],
        'max': [ 300 ]
      }
      });
      let updatecutofftext = function(intval) {
        let threshold = 20.0 / Math.pow(2, intval);
        $("#cutoff").text("E-value cutoff (" + threshold.toExponential() + "):");
        return threshold;
      };

      let changerslide = function(){
           let evalue = parseFloat(slider.noUiSlider.get());
           removed.restore();
           let threshold = updatecutofftext(evalue);
           removed = cy.filter('edge[E_value>' + threshold +']').remove();
      };
      updatecutofftext(slider_start);
      slider.noUiSlider.on('change', changerslide);
}); //end then
}

function makeWindow(objid, stopcb) {
  $(objid).draggable({
      handle: ".panel-heading",
      stop: function(evt, el) {}
  }).resizable({
      handles: "all",
      //ghost: true,
      stop: stopcb
  });
}

function locationAfterHash(str) {
    if(history.pushState) {
      history.pushState(null, null, str);
    }
    else {
      location.hash = str;
    }
}

function onCompressed(arr) {
    inflated = inflateAllResults(arr);
    stylejsn = JSON.parse(inflated[1]);
    initCy([JSON.parse(inflated[0]), stylejsn]);

}

function cyInit(nodesjson, stylejson){
    Promise.all(getPromisesForZipped([nodesjson, stylejson])).then(onCompressed);
}

function populateLens(data) {
  if(!("id" in data)){
    data = g_activeElement; // when focus lost, show selected (if any)
  }
  if("name" in data) {
    $("#lensheader").html("<b>"+data.name+"</b> <i style='font-size: 10px'>(you may move and resize the viewer window)</i>");
  }
  else {
    $("#lensheader").html("<b>Lens</b> <i style='font-size: 10px'>(you may move and resize the viewer window)</i>");
  }
  var lis = [];
  for(key in data) {
    lis.push(`<li><span><b>${key}</b></span><span>${data[key]}</span></li>`)
  }
  if((!("Res1" in data)) && "name" in data && data.name.startsWith("e")) {
    lis.push("<ul><a href='http://prodata.swmed.edu/ecod/complete/domain/" +
             data.name + "' target='_blank'>"+ data.name + "</a></ul>");
  }
  $("#datac").html("<ul>"+lis.join("\n")+"</ul>");
}

function initCy( then ){

  cy = window.cy = cytoscape({
    container: document.getElementById('cy'),
    elements: then[0].elements,
    hideEdgesOnViewport: true,
    hideLabelsOnViewport: true,
    style: then[1], //JSON.parse(then[1]),
    layout: {
        name : 'preset',
        //fit: true,
        padding: 30,
        randomize: false,
        componentSpacing: 100
    },
    ready: function(){}
  });

  homehtml = "index.html";
  cy.on('mouseover', 'edge', function(event) {populateLens(event.target._private.data);});
  cy.on('mouseout', 'edge', function(event) {populateLens({});});
  cy.on('mouseover', 'node', function(event) {populateLens(event.target._private.data);});
  cy.on('mouseout', 'node', function(event) {populateLens({});});

  cy.on('tap', 'node', function(event){
        var node = event.target; //cyTarget;
        g_activeElement = node.data();
        var nodeid = node.data().name;
        if(event.originalEvent.shiftKey) {
            var ref = homehtml+"#node="+nodeid;
            window.open(ref, '_blank');
        }
        else {
            showNode(cytoid,nodeid,node.data());
            locationAfterHash("#node="+nodeid);
        }
  });

  cy.on('tap', 'edge', function(event){
        var edge = event.target; //cyTarget;
        g_activeElement = edge.data();
        var edgeid = edge.id();
        var ed = edge.data();
        if(event.originalEvent.shiftKey) {
            var ref = homehtml+"#edge="+edgeid;
            window.open(ref, '_blank');
        }
        else {
            showEdge(edge.data());
            locationAfterHash("#edge="+edgeid);
        }
    });

    var evalue = parseFloat(slider_start);
    removed = cy.filter('edge[E_value>' + 20.0 / Math.pow(2, evalue) +']').remove();

    var nid = location.hash.split('node=')[1];
    if(nid) {
       showNode(-1,nid,-1);
    }
    else {
      var edgeid = location.hash.split('edge=')[1];
      if(edgeid) {
        showEdge(cy.filter('edge[id = "' + edgeid + '"]')[0].data());
      }
    }


}


function represent(structure, scheme) {
	try{
		structure.addRepresentation("cartoon", { color: scheme } );
 	}
	catch(err) {
		console.log("Can't cartoon");
		try {
			structure.addRepresentation("rope", { color: scheme } );
		}catch(err) {
			console.log("Can't rope");
			structure.addRepresentation("licorice", { color: scheme } );
		}
	}
}

function handleLoad(compList) {
    var scheme1 = NGL.ColormakerRegistry.addSelectionScheme(
                  [["magenta",resList[0]],["grey","*"]],"color1");
    represent(compList[0], scheme1);
    if(compList.length>1) {
      var scheme2 = NGL.ColormakerRegistry.addSelectionScheme(
                  [["red","*"]],"color2");
        var scheme2 = NGL.ColormakerRegistry.addSelectionScheme(
                   [["cyan",resList[1]],["silver","*"]],"color2");
	represent(compList[1], scheme2);

        compList[0].superpose(compList[1], true, resList[0], resList[1]);
    }
    compList[0].autoView();//centerView(false,resList[0]);
}

function resetStage(id) {
    stage.removeAllComponents();
    $("#toolbox-tools").show();
    $("#viewportheader").html("<b>"+id+"</b> <i style='font-size: 10px'>(you may move and resize the viewer window)</i>");
}

function gotFromPDB(pdbList) {
    var promiseList = []
    for(var i in pdbList) {
       var pdbData = pdbList[i];
       if(pdbData.length == 0) {
           console.log("Got empty PDB data");
            return;
        }
        promiseList.push(stage.loadFile(
                       new Blob( [ pdbData ], { type: 'text/plain'} ),
                       { ext: "pdb" } ));
    }
    if(pdbList.length == promiseList.length) {
        Promise.all(promiseList).then(handleLoad);
    }
}

function gotAlignment(ali_data) {
  arr = ali_data[0].split("\n");
  if(arr == "")
	 return;
  newRes1 = [];
  newRes2 = [];
  for(var i = 0; i < arr[1].length; ++i) {
      if(arr[1][i] == arr[3][i]) {
        var str = "<span class='ali_ident'>"+arr[1][i]+"</span>";
        newRes1.push(str)
        newRes2.push(str)
      }
      else {
        newRes1.push(arr[1][i]);
        newRes2.push(arr[3][i]);
      }
  }
  $("#ali").html(arr[0] + " " + newRes1.join("") + "<br/>" + arr[2] + " " + newRes2.join(""));
}

function showNode(id,nodeid,nodedata) {
    resetStage(nodeid);

    resList = "0";//[nodedata["totalrange"]];
    Promise.all([ $.get("cgi/getPDB.php?name="+nodeid) ])
           .then(gotFromPDB);
};

function showEdge(edgedata) {
  var names = edgedata["name"].split(" ");
  resetStage(names[0] + "-" + names[2]);
  //console.log(edgedata);
  if("Res1" in edgedata && "Res2" in edgedata) {
    resList = [edgedata["Res1"].replace(/\:/g,"-").replace(/,/g," "),
      	       edgedata["Res2"].replace(/\:/g,"-").replace(/,/g," ")];
  }
  else {
    resList = ["0","0"];
  }
  //console.log(resList);
  left = edgedata["name"].split(' ')[0];
  right = edgedata["name"].split(' ')[2];

  Promise.all([ $.get("cgi/getPDB.php?name="+left),
              $.get("cgi/getPDB.php?name="+right)])
        .then(gotFromPDB);
  //we may have the ali embedded in edgedata, rather then in a file
  if("Ali1" in edgedata && "Ali2" in edgedata) {
    var alis = [left, edgedata["Ali1"], right, edgedata["Ali2"]].join("\n");
    gotAlignment([alis]);
  }
  else { // try to fetch
    Promise.all([ $.get("cgi/getALI.php?name="+edgedata["shared_interaction"])])
        .then(gotAlignment);
      }
};

function help() {
  $( "#dialog" ).dialog(
  {
      width: 800,
      modal: true,
      buttons: {
        OK: function() {
          $( this ).dialog( "close" );
        }
      }
    });
}

function toggleLens(obj) {
  if(obj.checked) { $("#lenswindow").show(); }
  else { $("#lenswindow").hide();}
}

---
    appCaption: "Open in JMol"
    appBinary: jmol -s
    appContext: node
    suffix: jml
    scriptLines: |
        load FILE protein.pdb
        background white
        set chainCaseSensitive ON
        cartoon ONLY
        hide all
        define schain *:%node%
        select schain
        display schain
        color red
    type: script
--- 
    appCaption: "Open in JMol (Full structure)"
    appBinary: jmol -s
    appContext: node
    suffix: jml
    scriptLines: |
        load FILE protein.pdb
        background white
        set chainCaseSensitive ON
        cartoon ONLY
        color gray
        define schain *:%node%
        select schain
        color red
    type: script
--- 
appBinary: "vmd -e"
appCaption: "Open in VMD (Full structure)"
appContext: node
scriptLines: |
    menu files off
    menu files on
    display resetview
    mol addrep 0
    display resetview
    mol new {protein.pdb} type {pdb} first 0 last -1 step 1 waitfor 1
    animate style Loop
    menu files off
    menu color off
    menu color on
    color Display Background white
    menu color off
    menu graphics off
    menu graphics on
    mol modstyle 0 0 Cartoon 2.100000 24.000000 5.000000
    mol addrep 0
    mol showrep 0 1 0
    mol showrep 0 1 1
    mol color Name
    mol representation Cartoon 2.100000 24.000000 5.000000
    mol selection chain %node%
    mol material Opaque
    mol modrep 1 0
    mol modcolor 1 0 ColorID 1
    menu graphics off
suffix: log
type: script
--- 
appBinary: "vmd -e"
appCaption: "Open in VMD"
appContext: node
scriptLines: |
    menu files off
    menu files on
    display resetview
    mol addrep 0
    display resetview
    mol new {protein.pdb} type {pdb} first 0 last -1 step 1 waitfor 1
    animate style Loop
    menu files off
    menu color off
    menu color on
    color Display Background white
    menu color off
    menu graphics off
    menu graphics on
    mol modstyle 0 0 Cartoon 2.100000 24.000000 5.000000
    mol addrep 0
    mol showrep 0 1 0
    mol showrep 0 1 1
    mol color Name
    mol representation Cartoon 2.100000 24.000000 5.000000
    mol selection chain %node%
    mol material Opaque
    mol modrep 1 0
    mol modcolor 1 0 ColorID 1
    mol showrep 0 0 0
    menu graphics off
suffix: log
type: script

---
    appCaption: "Show in PyMol (full structure)"
    appBinary: pymolwin 
    suffix: pml
    appContext: node
    scriptLines: |
        cd %dir%
        load protein.pdb
        set ignore_case, 0	
        select chain_%node%, chain %node%
        hide all
        color gray30
        show cartoon
        color red, chain_%node%
        deselect
    type: script
--- 
    appCaption: "Show in PyMol"
    appBinary: pymolwin 
    suffix: pml
    appContext: node
    scriptLines: |
        cd %dir%
        load protein.pdb
        set ignore_case, 0	
        select chain_%node%, chain %node%
        hide all
        show cartoon, chain_%node%
        color red, chain_%node%
        zoom chain_%chain%
        deselect
    type: script
--- 
    appCaption: "Show in UCSF Chimera"
    appBinary: chimera
    suffix: cmd
    appContext: node
    scriptLines: |
        cd %dir%
        open protein.pdb
        ~ribbon #0:.*
        ~display #*:.*
        ribbon #0:.%node%
        color red #0:.%node%
    type: script
--- 
    appCaption: "Show in UCSF Chimera (Full structure)"
    appBinary: chimera
    suffix: cmd
    appContext: node
    scriptLines: |
        cd %dir%
        open protein.pdb
        color gray #0:.*
        color red #0:.%node%
    type: script

--- 
   appBinary: chimera
   appCaption: "Open in UCSF Chimera"
   appContext: edge
   dataMatrixFile: network.txt
   scriptLines: |
      cd %dir%
      open protein.pdb
      ~ribbon #0:.*
      ~display #*:.*
      ribbon #0:.%chain1%
      color gray #0:.%chain1%
      color cyan #0:%H_START%%res1%,%H_END%0.%chain1%
      color gray #0:.%chain2%
      ribbon #0:.%chain2%
      color orange #0:%H_START%%res2%,%H_END%0.%chain2%
      focus
   suffix: cmd
   type: script
--- 
   appBinary: pymolwin
   appCaption: "Open in PyMol"
   appContext: edge
   dataMatrixFile: network.txt
   scriptLines: |
      set ignore_case, 0	
      load protein.pdb
      select chain1_%chain1%, chain %chain1%
      select ifres1, (chain1_%chain1% and resi %H_START%%res1%,%H_END%none) 
      select chain2_%chain2%, chain %chain2%
      select ifres2, (chain2_%chain2% and resi %H_START%%res2%,%H_END%none) 
      hide all
      show cartoon, chain1_%chain1% 
      color cyan, chain1_%chain1%
      show spheres, ifres1
      color blue, ifres1
      show cartoon, chain2_%chain2%    
      color orange, chain2_%chain2%
      show spheres, ifres2
      color red, ifres2
      orient chain1_%chain1% chain2_%chain2%
      deselect
   suffix: pml
   type: script
--- 
   appBinary: pymolwin
   appCaption: "Open in PyMol (Full structure)"
   appContext: edge
   dataMatrixFile: network.txt
   scriptLines: |
      set ignore_case, 0	
      load protein.pdb
      select chain1_%chain1%, chain %chain1%
      select ifres1, (chain1_%chain1% and resi %H_START%%res1%,%H_END%none) 
      select chain2_%chain2%, chain %chain2%
      select ifres2, (chain2_%chain2% and resi %H_START%%res2%,%H_END%none) 
      hide all
      show cartoon
      color gray
      show cartoon, chain1_%chain1% 
      color cyan, chain1_%chain1%
      show spheres, ifres1
      color blue, ifres1
      show cartoon, chain2_%chain2%   
      color orange, chain2_%chain2%
      show spheres, ifres2
      color red, ifres2
      orient chain1_%chain1% chain2_%chain2%
      deselect
   suffix: pml
   type: script
---
    appCaption: "Open in JMol"
    dataMatrixFile: network.txt
    appBinary: jmol -s
    appContext: edge
    suffix: jml
    scriptLines: |
        load FILE protein.pdb
        background white
        set chainCaseSensitive ON
        cartoon ONLY
        define chain1_%chain1% none
        %H_START%define chain1_%chain1% chain1_%chain1% or (%res1%:%chain1%)
        %H_END%
        select chain1_%chain1%
        color cyan
        define chain2_%chain2% none
        %H_START%define chain2_%chain2% chain2_%chain2% or (%res2%:%chain2%)
        %H_END%
        select chain2_%chain2%
        color magenta
    type: script
--- 
   appBinary: vmd -e
   appCaption: "Open in VMD"
   appContext: edge
   dataMatrixFile: network.txt
   suffix: log
   scriptLines: |
    menu files off
    menu files on
    display resetview
    mol addrep 0
    display resetview
    mol new {protein.pdb} type {pdb} first 0 last -1 step 1 waitfor 1
    animate style Loop
    menu files off
    menu color off
    menu color on
    color Display Background white
    mol addrep 0
    mol modselect 1 0 chain %chain1%
    mol modstyle 1 0 Cartoon 2.100000 24.000000 5.000000
    mol modcolor 1 0 ColorID 0
    mol color ColorID 0
    mol representation Cartoon 2.100000 24.000000 5.000000
    mol selection chain %chain1% 
    mol material Opaque
    mol addrep 0
    mol modselect 2 0 chain %chain2%
    mol modcolor 2 0 ColorID 1
    mol color ColorID 1
    mol representation Cartoon 2.100000 24.000000 5.000000
    mol selection chain %chain2%
    mol material Opaque
    mol modrep 2 0
    mol color ColorID 1
    mol representation Cartoon 2.100000 24.000000 5.000000
    mol selection chain %chain2%
    mol material Opaque
    mol addrep 0
    mol modselect 3 0 chain %chain2% and (%H_START%resid %res2% or %H_END% resid 99999)
    mol modstyle 3 0 VDW 1.000000 12.000000    
    mol representation Cartoon 2.100000 24.000000 5.000000
    mol selection chain %chain1%
    mol material Opaque
    mol addrep 0
    mol modselect 4 0 chain %chain1% and (%H_START%resid %res1% or %H_END% resid 999999)
    mol modstyle 4 0 VDW 1.000000 12.000000
    mol modcolor 4 0 ColorID 0
    mol showrep 0 0 0
    menu graphics off
    menu files off
   type: script




